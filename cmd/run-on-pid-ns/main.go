package main

import (
	"os"
	"os/exec"
	"os/signal"
	"strings"

	"golang.org/x/sys/unix"
)

func main() {
	err := unix.Unshare(unix.CLONE_NEWPID | unix.CLONE_NEWUTS | unix.CLONE_NEWNS)
	if err != nil {
		panic(err)
	}

	// So next executed process (children) will have PID 1
	cmd := exec.Command(strings.Join(os.Args[1:], " ")) // #nosec
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err = cmd.Start(); err != nil {
		panic(err)
	}

	go forwardSignals(cmd.Process)

	if err = cmd.Wait(); err != nil {
		println(err.Error())
	} else {
		println("Process exited OK")
	}
}

func forwardSignals(process *os.Process) {
	channel := make(chan os.Signal, 10)
	signal.Notify(channel, unix.SIGINT, unix.SIGTERM, unix.SIGUSR1, unix.SIGUSR2)

	for sig := range channel {
		_ = process.Signal(sig)
	}
}
