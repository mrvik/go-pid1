// service-launcher holds needed things to recognize and launch services
package main

import (
	"context"
	"fmt"
	"init/fs"
	"init/plugins"
	"init/services"
	"init/socket"
	"init/util"
	"io"
	"os"
	"os/signal"
	"syscall"
	"time"

	"cdr.dev/slog"
)

const PluginsDir = "/etc/service-launcher/plugins"

var (
	logger = util.NewLog("service-launcher")
)

func main() {
	if err := Run(); err != nil {
		logger.Critical(context.Background(), "Fatal error", slog.Error(err))
		os.Exit(1)
	}
}

func Run() error {
	// Skip boot allows callers to avoid boot tasks like mounting filesystems.
	// This setting can be useful in container environments where the setup is already done.
	skipBoot := len(os.Args) > 1 && os.Args[1] == "skipBoot"

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sctx, scancel := context.WithCancel(ctx)
	deadChildrenChannel := make(chan bool, 5)
	go watchSignals(ctx, scancel, deadChildrenChannel)

	if !skipBoot {
		logger.Info(sctx, "Mounting filesystems")
		mounter := fs.NewMounter()
		defer mounter.UmountAll(ctx)

		if err := mounter.MountAll(sctx); err != nil {
			return fmt.Errorf("mount filesystems: %w", err)
		}

		logger.Info(sctx, "Setting hostname and keymap")
		if err := setHostname(); err != nil {
			return fmt.Errorf("set hostname: %w", err)
		}

		if err := setKeymap(); err != nil {
			return fmt.Errorf("set keymap: %w", err)
		}
	} else {
		logger.Info(sctx, "Boot tasks were skipped")
	}

	slPlugins := plugins.ReadAll(sctx, PluginsDir)

	logger.Info(sctx, "Starting all services...")

	svc, err := services.NewServiceManager(sctx, deadChildrenChannel, slPlugins)
	if err != nil {
		return fmt.Errorf("create service manager: %w", err)
	}

	if err = svc.StartAll(sctx); err != nil {
		return fmt.Errorf("start al services: %w", err)
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		logger.Info(ctx, "Stopping all services...")
		svc.StopAll(ctx)
	}()

	commands := map[string]socket.CommandHandler{
		// Power managament
		"reboot":   handleReboot,
		"poweroff": handleReboot,
		"kexec":    handleReboot,

		// Manage services
		"list":    svc.HandleListCommand,
		"start":   svc.HandleServiceCommand,
		"stop":    svc.HandleServiceCommand,
		"restart": svc.HandleServiceCommand,
		"status":  svc.HandleServiceCommand,
		"dump":    svc.HandleServiceCommand,

		"reload": svc.HandleReloadCommand,

		// Get journal entries
		"journal":   svc.HandleJournalCommands,
		"journal-f": svc.HandleJournalCommands,
	}

	sckt, err := socket.NewListener(ctx, commands)
	if err != nil {
		return fmt.Errorf("create listener: %w", err)
	}

	defer sckt.Close(ctx)

	go sckt.Listen(sctx)

	<-sctx.Done()

	// Deferred functions will do the teardown
	logger.Info(ctx, "Exiting service manager")

	return nil
}

func watchSignals(ctx context.Context, cancel context.CancelFunc, deadChild chan<- bool) {
	defer cancel()
	defer close(deadChild)

	ch := make(chan os.Signal, 1)
	defer close(ch)
	signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGCHLD)

	defer signal.Stop(ch)

	for {
		select {
		case <-ctx.Done():
			return
		case sig := <-ch:
			switch sig {
			case syscall.SIGCHLD:
				// Dead body reported
				deadChild <- true
			default:
				cancel()
			}
		}
	}
}

func handleReboot(command, _ string, respond io.WriteCloser) {
	defer respond.Close()

	var signal os.Signal

	switch command {
	case "reboot":
		signal = syscall.SIGINT
	case "poweroff":
		signal = syscall.SIGUSR1
	case "kexec":
		signal = syscall.SIGUSR2
	default:
		_, _ = respond.Write([]byte("Command " + command + " does not correspond with any signal"))
	}

	if signal == nil {
		return
	}

	fmt.Fprintf(respond, "Killing init with signal %s\n", signal.String())
	// PID 1 must exist
	process, err := os.FindProcess(1)
	if err != nil {
		fmt.Fprintf(respond, "Error finding parent: %s\n", err)
	}

	if err = process.Signal(signal); err != nil {
		fmt.Fprintf(respond, "Cannot kill parent (%d) with signal %s: %s\n", process.Pid, signal.String(), err)
	} else {
		fmt.Fprintf(respond, "Done\n")
	}
}
