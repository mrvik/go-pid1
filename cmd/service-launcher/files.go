package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"golang.org/x/sys/unix"
)

func setHostname() error {
	hostnameBytes, err := ioutil.ReadFile("/etc/hostname")
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
		} else {
			err = fmt.Errorf("cannot read vconsole conf: %w", err)
		}

		return err
	}

	hostname := bytes.Trim(hostnameBytes, "\n ")

	return unix.Sethostname(hostname)
}

func setKeymap() error {
	vconsole, err := ioutil.ReadFile("/etc/vconsole.conf")
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
		} else {
			err = fmt.Errorf("cannot read vconsole conf: %w", err)
		}

		return err
	}

	var keymap []byte

	for _, line := range bytes.Split(vconsole, []byte{'\n'}) {
		if bytes.HasPrefix(line, []byte("KEYMAP=")) {
			keymap = bytes.TrimPrefix(line, []byte("KEYMAP="))

			break
		}
	}

	if keymap == nil {
		return nil
	}

	return exec.Command("loadkeys", string(keymap)).Run() // #nosec
}
