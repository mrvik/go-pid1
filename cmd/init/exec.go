package main

import (
	"fmt"
	"os"
	"os/exec"
)

func launchServiceDaemon(extraArgs []string) (*os.Process, error) {
	os.Setenv("PATH", os.Getenv("PATH")+":/usr/bin:/usr/local/bin")

	path, err := exec.LookPath("service-launcher")
	if err != nil {
		return nil, fmt.Errorf("cannot find service launcher in PATH: %w", err)
	}

	cmd := exec.Cmd{
		Path: path,
		Args: append([]string{
			"service-launcher",
		}, extraArgs...),
		Env:    append(os.Environ(), "PATH=/bin:/usr/bin:/sbin/usr/sbin:/usr/local/bin"),
		Stdin:  nil,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}

	if err = cmd.Start(); err != nil {
		err = fmt.Errorf("cannot start Service Daemon: %w", err)
	}

	return cmd.Process, err
}
