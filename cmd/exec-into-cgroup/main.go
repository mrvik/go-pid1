package main

import (
	"os"
	"os/exec"

	"golang.org/x/sys/unix"
)

const incomingFd = 3

// Needs a handle to the cgroup.procs file inside the cgroup.
// This process adds itself to the needed cgroup and then execs the process in args.
func main() {
	// 0, 1, 2 fd are reserved. Next FD is 3
	handle := os.NewFile(incomingFd, "cgroup.procs")
	if handle == nil {
		panic("File descriptor is invalid")
	}

	if _, err := handle.Write([]byte("0")); err != nil {
		panic("Cannot add process to cgroup: " + err.Error())
	}

	handle.Close()

	cmd, err := exec.LookPath(os.Args[0])
	if err != nil {
		panic(err)
	}

	if err := unix.Exec(cmd, os.Args, os.Environ()); err != nil {
		panic(err)
	}
}
