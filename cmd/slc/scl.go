package main

import (
	"io"
	"net"
	"os"
	"strings"

	"init/socket"
)

func main() {
	conn, err := net.Dial("unix", socket.SocketAddr)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	cmd := strings.Join(os.Args[1:], " ")
	println(cmd)

	if _, err = conn.Write([]byte(cmd)); err != nil {
		panic(err)
	}

	var buf [2048]byte
	if _, err = io.CopyBuffer(os.Stdout, conn, buf[:]); err != nil {
		panic(err)
	}
}
