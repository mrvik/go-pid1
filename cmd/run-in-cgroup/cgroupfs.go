package main

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/fsnotify/fsnotify"
	"golang.org/x/sys/unix"
)

// Errors.
var (
	ErrIsNotDir  = errors.New("cgroup mount path is not a dir")
	ErrNonUnique = errors.New("cgroup name is not unique")
	ErrNoCommand = errors.New("command name not supplied")
)

// Parameters.
const (
	ProcessTimeout = time.Second * 20
)

type Cgroup struct {
	path string
}

func NewCgroup(mount, name string) (*Cgroup, error) {
	cg := Cgroup{
		path: filepath.Join(mount, name),
	}

	stat, err := os.Stat(mount)
	if err != nil {
		return nil, fmt.Errorf("cgroup mount doesn't exist: %w", err)
	} else if !stat.IsDir() {
		return nil, fmt.Errorf("%q: %w", mount, ErrIsNotDir)
	}

	// Must not exist a cgroup with the same name
	_, err = os.Stat(cg.path)

	switch {
	case os.IsNotExist(err):
	case err == nil:
		err = ErrNonUnique

		fallthrough
	default:
		return nil, fmt.Errorf("looking for cgroup with the same name %q: %w", cg.path, err)
	}

	// Now it's safe to create a new cgroup
	if err = os.Mkdir(cg.path, 0o644); err != nil {
		return nil, fmt.Errorf("cannot create cgroup at %q: %w", cg.path, err)
	}

	return &cg, nil
}

// Run a process inside of the current cgroup.
// Run will block until the main process exits.
func (cg *Cgroup) Run(cmdline []string) error {
	if len(cmdline) == 0 {
		return ErrNoCommand
	}

	eic, err := exec.LookPath("exec-into-cgroup")
	if err != nil {
		return fmt.Errorf("cannot find required util: %w", err)
	}

	procsHandle, err := os.OpenFile(filepath.Join(cg.path, "cgroup.procs"), os.O_APPEND|os.O_WRONLY, 0)
	if err != nil {
		return fmt.Errorf("cannot open handle to cgroup.procs: %w", err)
	}

	cmd := exec.Cmd{
		Path: eic,
		Args: cmdline,
		Env:  os.Environ(),

		ExtraFiles: []*os.File{
			procsHandle, // FD == 3
		},

		Stdin:  os.Stdin,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}

	if err = cmd.Run(); err != nil {
		return fmt.Errorf("error on child process %q: %w", cmdline[0], err)
	}

	return nil
}

// Kill all processes inside the cgroup (gracefully).
func (cg *Cgroup) KillAll() error {
	pidNums, err := cg.getPids()
	if err != nil {
		return err
	}

	if len(pidNums) == 0 {
		return nil
	}

	wait := make([]*os.Process, 0, len(pidNums))

	for _, pid := range pidNums {
		proc, _ := os.FindProcess(pid)
		if err := proc.Signal(unix.SIGTERM); err == nil {
			wait = append(wait, proc)
		}
	}

	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	go killOrCtx(ctx, wait, ProcessTimeout)

	for _, proc := range wait {
		_, _ = proc.Wait()
	}

	return nil
}

func killOrCtx(ctx context.Context, procs []*os.Process, deadline time.Duration) {
	select {
	case <-ctx.Done():
		return
	case <-time.After(deadline):
	}

	for _, proc := range procs {
		_ = proc.Signal(unix.SIGKILL)
	}
}

func (cg *Cgroup) getPids() (pidNums []int, err error) {
	pids, err := ioutil.ReadFile(filepath.Join(cg.path, "cgroup.procs"))
	if err != nil {
		err = fmt.Errorf("cannot get pids in cgroup: %w", err)

		return
	}

	pidNums, err = atoiAll(strings.Split(string(pids), "\n"))
	if err != nil {
		err = fmt.Errorf("error parsing pids: %w", err)
	}

	return
}

func atoiAll(strs []string) (out []int, err error) {
	ln := len(strs)
	if ln == 0 {
		return
	}

	out = make([]int, 0, ln)

	for _, str := range strs {
		if str == "" {
			continue
		}

		var num int

		if num, err = strconv.Atoi(str); err != nil {
			break
		}

		out = append(out, num)
	}

	return
}

// Call KillAll and remove cgroup.
func (cg *Cgroup) Teardown() {
	if err := cg.KillAll(); err != nil {
		panic(err)
	}

	// Now cgroup is empty (or should be) and we can safely remove it
	if err := os.Remove(cg.path); err != nil {
		panic(err)
	}
}

func (cg *Cgroup) WaitLoop(ctx context.Context, sigchld <-chan bool) int {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		panic(err)
	}

	defer watcher.Close()

	if err = watcher.Add(filepath.Join(cg.path, "cgroup.events")); err != nil {
		panic(err)
	}

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case _, ok := <-watcher.Events:
			if !ok {
				break loop
			}
		case <-sigchld:
		}
		if code := cg.checkProcs(); code != -1 {
			return code
		}
	}

	return 0
}

func (cg *Cgroup) checkProcs() (code int) {
	var codes []int

	for {
		var status unix.WaitStatus
		rtpid, _ := unix.Wait4(-1, &status, unix.WNOHANG, nil)

		if rtpid <= 0 {
			break
		}

		codes = append(codes, cg.processExited(status))
	}

	if len(codes) == 0 {
		// We have no exit codes, but maybe cgroup is empty
		if pids, err := cg.getPids(); err == nil && len(pids) == 0 {
			return 0 // Exit OK
		}
		return -1
	}

	for _, c := range codes {
		if c > code {
			code = c
		}
	}

	return
}

// This function will exit if last process exited with non-0 status.
func (cg *Cgroup) processExited(status unix.WaitStatus) int {
	pids, err := cg.getPids()
	if err != nil { // Cannot read pids. This shouldn't happen
		panic("Cannot read pids after child exited: " + err.Error())
	}

	if len(pids) != 0 { // Other processes are running, we won't kill them
		return -1
	}

	// At this point we have a process exited and no more pids on cgroup. We have to exit from here
	return status.ExitStatus()
}
