package plugins

import (
	"context"
	"errors"
	"fmt"
	"init/util"
	"os"
	"path"
	"plugin"

	"cdr.dev/slog"
)

const PluginSymbol = "Register"

type PluginRegisterSymbol func(into *PluginList) error

var logger = util.NewLog("plugins")

var (
	// ErrBadSymbol is returned when the symbol exists buthas the wrong type.
	ErrBadSymbol = errors.New("bad symbol type")
)

func ReadAll(ctx context.Context, from string) PluginList {
	logger.Info(ctx, "Reading plugins", slog.F("directory", from))

	entries, _ := os.ReadDir(from)

	var pl PluginList

	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}

		name := entry.Name()
		logger := logger.With(slog.F("plugin", name))

		p, err := plugin.Open(path.Join(from, name))
		if err != nil {
			logger.Error(ctx, err.Error(), slog.F("plugin", name))

			continue
		}

		sym, _ := p.Lookup(PluginSymbol)
		if sym == nil {
			logger.Warn(ctx, fmt.Sprintf("Can't find symbol %s in plugin", PluginSymbol))

			continue
		}

		switch v := sym.(type) {
		case *PluginRegisterSymbol:
			err = (*v)(&pl)
		default:
			err = fmt.Errorf("%w: symbol is %T", ErrBadSymbol, v)
		}

		if err != nil {
			logger.Error(ctx, fmt.Sprintf("Ignoring plugin: %s", err))
		} else {
			logger.Info(ctx, "Plugin loaded")
		}
	}

	logger.Info(ctx, "Plugin summary", pl.SummaryFields()...)

	return pl
}
