package plugins

type ServicePlugin interface {
	SupervisorStarted()
	NotifyStart(serviceName string)
	NotifyDeath(serviceName string)
	SupervisorStopping()
}
