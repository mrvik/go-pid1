package plugins

import (
	"init/journal"
)

type PluginList struct {
	JournalPlugins []journal.JournalPlugin
	ServicePlugins []ServicePlugin
}

func (pl PluginList) SummaryFields() []interface{} {
	return []interface{}{
		"JournalPlugins", len(pl.JournalPlugins),
		"ServicePlugins", len(pl.ServicePlugins),
	}
}
