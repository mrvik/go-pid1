package fs

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"strings"

	"cdr.dev/slog"
)

func (*Mounter) getCurrentMounts(ctx context.Context) (FilesystemSlice, error) {
	slice, err := parseFile(ctx, "/proc/self/mounts")
	if err != nil {
		return nil, err
	}

	swap, err := parseSwapMounts()
	if err != nil {
		return nil, err
	}

	return append(slice, swap...), nil
}

func (*Mounter) getFstab(ctx context.Context) (FilesystemSlice, error) {
	return parseFile(ctx, "/etc/fstab")
}

func parseFile(ctx context.Context, filename string) (slice FilesystemSlice, err error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		err = fmt.Errorf("cannot identify mounts: %w", err)
		return
	}

	// Map special filesystems
	specialFs := make(map[string]struct{})

	for _, fs := range initialFilesystems {
		specialFs[fs.Dest] = struct{}{}
	}

	lines := strings.Split(string(content), "\n")
	slice = make(FilesystemSlice, 0, len(lines))

	for ln, line := range lines {
		if len(line) < 2 || line[0] == '#' {
			continue
		}

		var (
			fields          [4]string
			extractedFields = strings.Fields(line)
		)

		if len(extractedFields) != 6 { // We're only using 4 fields, but fstab must have 6 so we enforce it
			logger.Warn(ctx, fmt.Sprintf("Bad format: line has %d fields", len(extractedFields)), slog.F("line", ln), slog.F("file", filename))
			continue
		}

		copy(fields[:], extractedFields)

		slice = append(slice, NewFilesystem(fields))
	}

	return
}

func parseSwapMounts() (FilesystemSlice, error) {
	content, err := ioutil.ReadFile("/proc/swaps")
	if err != nil {
		return nil, err
	}

	var slice FilesystemSlice

	for i, line := range bytes.Split(content, []byte{'\n'}) {
		if i == 0 {
			continue // Skip headers
		}

		fields := bytes.Fields(line)
		if len(fields) < 1 {
			continue
		}

		slice = append(slice, FilesystemSpec{
			Source:       string(fields[0]),
			Type:         "swap",
			AvoidRemount: true, // Cannot change options on swap, so remount is useless and unsupported
		})
	}

	return slice, nil
}
