package fs

type FilesystemSlice []FilesystemSpec

func (fsl FilesystemSlice) Len() int {
	return len(fsl)
}

// FS1 has to be umounted before FS2 if it's path is bigger
func (fsl FilesystemSlice) Less(i1, i2 int) bool {
	return len(fsl[i1].Dest) > len(fsl[i2].Dest)
}

func (fsl FilesystemSlice) Swap(i1, i2 int) {
	fsl[i1], fsl[i2] = fsl[i2], fsl[i1]
}

// Return Root mount present on FilesystemSlice or nil
func (fst FilesystemSlice) Root() *FilesystemSpec {
	for _, fs := range fst {
		if fs.Dest == "/" {
			return &fs
		}
	}

	return nil
}
