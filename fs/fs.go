// Package fs implements filesystem handling (reading from fstab and so on)
package fs

import (
	"context"
	"errors"
	"fmt"
	"init/util"
	"sort"
	"strings"
	"unsafe"

	"cdr.dev/slog"
	"golang.org/x/sys/unix"
)

// Errors.
var (
	ErrSwapon = errors.New("error doint swapon")
)

var (
	logger = util.NewLog("filesystems")
	// Equivalence between string flags and mount call flags
	flagsEquivalence = map[string]uintptr{
		"dirsync":     unix.MS_DIRSYNC,
		"lazytime":    unix.MS_LAZYTIME,
		"noatime":     unix.MS_NOATIME,
		"nodev":       unix.MS_NODEV,
		"nodiratime":  unix.MS_NODIRATIME,
		"noexec":      unix.MS_NOEXEC,
		"nosuid":      unix.MS_NOSUID,
		"ro":          unix.MS_RDONLY,
		"relatime":    unix.MS_RELATIME,
		"silent":      unix.MS_SILENT,
		"strictatime": unix.MS_STRICTATIME,
		"sync":        unix.MS_SYNCHRONOUS,

		// Defaults
		"rw":       0,
		"defaults": 0,
	}
)

type FilesystemSpec struct {
	Source, Dest string
	Type         string
	Data         string

	AvoidRemount bool
}

func (fsp FilesystemSpec) Flags() (flags uintptr) {
	for _, data := range strings.Split(fsp.Data, ",") {
		flag, exists := flagsEquivalence[data]
		if exists {
			flags |= flag
		}
	}
	return
}

// JustData will trim all mount flags and preflight hooks
func (fsp FilesystemSpec) JustData() string {
	all := strings.Split(fsp.Data, ",")
	nf := make([]string, 0, len(all))

	for _, flag := range all {
		if _, e := flagsEquivalence[flag]; e || isPreflight(flag) != nil {
			continue
		}

		nf = append(nf, flag)
	}

	return strings.Join(nf, ",")
}

// Fill an spec from fields
func NewFilesystem(fields [4]string) FilesystemSpec {
	return FilesystemSpec{
		Source: correctSpec(fields[0]),
		Dest:   fields[1],
		Type:   fields[2],
		Data:   fields[3],
		// Other fields are ignored as we're not handling fsck (do it yourself)
	}
}

const commonOptions = "rw,nosuid,relatime"

// This array is initialized with base filesystems
// Filesystems on /etc/fstab are mounted on call to `mount -a`.
var initialFilesystems = []FilesystemSpec{
	{
		Source:       "proc",
		Dest:         "/proc",
		Type:         "proc",
		Data:         commonOptions + ",nodev,noexec",
		AvoidRemount: true,
	},
	{
		Source: "binfmt",
		Dest:   "/proc/sys/fs/binfmt_misc",
		Type:   "binfmt_misc",
		Data:   "rw,relatime",
	},
	{
		Source:       "sys",
		Dest:         "/sys",
		Type:         "sysfs",
		Data:         commonOptions + ",noexec,nodev",
		AvoidRemount: true,
	},
	{
		Source:       "dev",
		Dest:         "/dev",
		Type:         "devtmpfs",
		Data:         commonOptions + ",inode64,mode=755",
		AvoidRemount: true,
	},
	{
		Source: "shm",
		Dest:   "/dev/shm",
		Type:   "tmpfs",
		Data:   commonOptions + ",nodev,inode64,x-mount.mkdir",
	},
	{
		Source:       "devpts",
		Dest:         "/dev/pts",
		Type:         "devpts",
		Data:         commonOptions + ",gid=5,mode=620,ptmxmode=000,x-mount.mkdir",
		AvoidRemount: true,
	},
	{
		Source:       "run",
		Dest:         "/run",
		Type:         "tmpfs",
		Data:         commonOptions + ",nodev,mode=755,inode64",
		AvoidRemount: true,
	},
	{
		Source: "securityfs",
		Dest:   "/sys/kernel/security",
		Type:   "securityfs",
		Data:   "rw,relatime,x-mount.mkdir",
	},
	{
		Source: "cgroup2",
		Dest:   "/sys/fs/cgroup",
		Type:   "cgroup2",
		Data:   commonOptions + ",noexec,nsdelegate,memory_recursiveprot",
	},
}

type Mounter []FilesystemSpec

func NewMounter() Mounter {
	mounter := make(Mounter, len(initialFilesystems))
	copy(mounter, initialFilesystems)

	return mounter
}

func (m Mounter) MountAll(ctx context.Context) error {
	currentMap := make(map[string]*FilesystemSpec)
	fstabMounts, err := m.getFstab(ctx)
	if err != nil {
		return err
	}

	slices := []FilesystemSlice{
		{
			initialFilesystems[0],
		},
		FilesystemSlice(m),
		fstabMounts,
	}

	for _, slice := range slices {
		if err := m.MountSlice(slice, currentMap); err != nil {
			return err
		}

		// Refresh current mounts
		currentMounts, err := m.getCurrentMounts(ctx)
		if err != nil {
			return err
		}

		for _, mount := range currentMounts {
			currentMap[mount.Dest] = &mount
		}
	}

	return m.MountSlice(fstabMounts, currentMap)
}

// Mount all filesystems in slice and add new mounts to currentMap
func (m Mounter) MountSlice(slice []FilesystemSpec, currentMap map[string]*FilesystemSpec) (err error) {
	for _, mount := range slice {
		current, mounted := currentMap[mount.Dest]
		if !mounted {
			if err = m.mount(mount, 0); err != nil {
				return fmt.Errorf("error mounting %s: %w", mount.Dest, err)
			}

			currentMap[mount.Dest] = &mount

			continue
		}

		if mount.AvoidRemount || mount.Type == "swap" { // Avoid remounting swap (unsupported)
			continue
		}

		if diffMounts(*current, mount) {
			if err = m.mount(mount, unix.MS_REMOUNT); err != nil {
				return fmt.Errorf("error remounting %s: %w", mount.Dest, err)
			}

			currentMap[mount.Dest] = &mount
		}
	}

	return nil
}

func (m Mounter) mount(mount FilesystemSpec, flags uintptr) error {
	preflight := mount.Preflight()
	for flag, hook := range preflight {
		if err := hook(flag, mount); err != nil {
			return err
		}
	}
	if mount.Type == "swap" {
		return m.swapon(mount, flags) // unix.Mount doesn't handle swapon calls (mount(2) doesn't either)
	}
	return unix.Mount(mount.Source, mount.Dest, mount.Type, mount.Flags()|flags, mount.JustData())
}

func (m Mounter) swapon(mount FilesystemSpec, flags uintptr) error {
	stringPtr, err := unix.BytePtrFromString(mount.Source)
	if err != nil {
		return err
	}

	_, _, e1 := unix.Syscall(unix.SYS_SWAPON, uintptr(unsafe.Pointer(stringPtr)), flags, 0)
	if e1 != 0 {
		err = fmt.Errorf("%w: %s", ErrSwapon, unix.ErrnoName(e1))
	}
	return err
}

func diffMounts(current, mount FilesystemSpec) bool {
	if mount.Source != current.Source {
		return false // Not comparable
	}

	switch {
	case mount.Type != current.Type,
		mount.Data != current.Data:
		return true
	}

	return false
}

// So we have to find all mounts except those ones considered "special"
// whose won't be unmounted.
// Finally we remount / as read-only
func (m Mounter) UmountAll(ctx context.Context) error {
	var (
		umountFilesystems FilesystemSlice
		rootFs            *FilesystemSpec
		err               error
	)

	if umountFilesystems, err = m.getCurrentMounts(ctx); err != nil {
		return err
	}

	sort.Sort(umountFilesystems)

	for _, fs := range umountFilesystems {
		if fs.Dest == "/" || fs.Dest == "none" {
			continue
		}

		err := unix.Unmount(fs.Dest, 0)
		if err != nil {
			logger.Error(ctx, fmt.Sprintf("Unmount error: %s", err), slog.F("path", fs.Dest))
		}
	}

	if rootFs = umountFilesystems.Root(); rootFs == nil {
		return nil
	}

	// Now remount / as ro
	return m.mount(*rootFs, unix.MS_REMOUNT|unix.MS_RDONLY)
}

func replaceData(from, replace, replacement string, insert bool) string {
	flags := strings.Split(from, ",")
	found := -1

	for i, flag := range flags {
		if flag != replace {
			continue
		}
		found = i
		break
	}

	if found < 0 {
		flags = append(flags, replacement)
	} else {
		flags[found] = replacement
	}

	return strings.Join(flags, ",")
}

func correctSpec(source string) string {
	spl := strings.SplitN(source, "=", 2)
	if len(spl) < 2 {
		return source // Nothing to do
	}

	return "/dev/disk/by-" + strings.ToLower(spl[0]) + "/" + spl[1]
}
