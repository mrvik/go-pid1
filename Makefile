OUTDIR:=out
PLUGINS_OUTDIR:=plugins-out
DESTDIR:=/usr/local

sources=$(wildcard cmd/*)
dests=$(foreach thing,$(sources),$(OUTDIR)/$(notdir $(thing)))
plugins=$(wildcard example-plugins/*)
pluginDests=$(foreach thing,$(plugins),$(PLUGINS_OUTDIR)/$(notdir $(thing)))

.PHONY: all
all: $(OUTDIR) $(dests)

.PHONY: plugins
plugins: $(pluginDests)

.PHONY: install
install:
	install -Dm 0711 $(dests) "$(DESTDIR)/bin/"

$(OUTDIR)/%: $(wildcard */*.go) $(wildcard cmd/*/*.go)
	go build -v -trimpath -ldflags '-s -w' -tags netgo -o "$(OUTDIR)/$(notdir $@)" "./cmd/$(notdir $@)"

$(PLUGINS_OUTDIR)/%: $(wildcard */*.go) $(wildcard example-plugins/*/*.go)
	go build -v -trimpath -tags netgo -ldflags='-s -w' -buildmode=plugin -o "$(PLUGINS_OUTDIR)/$(notdir $@)" "./example-plugins/$(notdir $@)"

$(OUTDIR):
	mkdir -p $(OUTDIR)

.PHONY: clean
clean:
	rm -rf "$(OUTDIR)"
	rm -rf "$(PLUGINS_OUTDIR)"
