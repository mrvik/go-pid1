module init

go 1.15

require (
	cdr.dev/slog v1.6.1
	github.com/MicahParks/keyfunc v1.9.0
	github.com/charmbracelet/lipgloss v0.8.0 // indirect
	github.com/fsnotify/fsnotify v1.6.0
	github.com/go-logfmt/logfmt v0.6.0
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/tv42/httpunix v0.0.0-20191220191345-2ba4b9c3382c
	go.opentelemetry.io/otel v1.18.0 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/sync v0.3.0
	golang.org/x/sys v0.12.0
	gopkg.in/yaml.v3 v3.0.1
)
