package util

import (
	"context"
	"fmt"
	"io"
	"os"
	"strconv"
	"sync"

	"cdr.dev/slog"
	"cdr.dev/slog/sloggers/sloghuman"
	"github.com/go-logfmt/logfmt"
	"golang.org/x/sys/unix"
)

var isDebug = maybeBool(os.Getenv("GP1_DEBUG"))

func NewLog(name string) slog.Logger {
	level := slog.LevelInfo
	if isDebug {
		level = slog.LevelDebug
	}

	return slog.Make(GetSink()).Named(name).Leveled(level)
}

func GetSink() slog.Sink {
	if isTerminal(os.Stdout.Fd()) {
		return sloghuman.Sink(os.Stdout)
	}

	return NewLogfmtSink(os.Stdout)
}

func isTerminal(fd uintptr) bool {
	_, err := unix.IoctlGetTermios(int(fd), unix.TCGETS)

	return err == nil
}

func maybeBool(str string) bool {
	b, _ := strconv.ParseBool(str)

	return b
}

type LogfmtSink struct {
	writer *logfmt.Encoder
	under  io.Writer
	mtx    *sync.Mutex
}

func NewLogfmtSink(to io.Writer) LogfmtSink {
	return LogfmtSink{
		writer: logfmt.NewEncoder(to),
		under:  to,
		mtx:    new(sync.Mutex),
	}
}

type kvPair [2]interface{}

func (lf LogfmtSink) LogEntry(ctx context.Context, e slog.SinkEntry) {
	fields := []kvPair{
		{"ts", e.Time},
		{"level", e.Level.String()},
		{"msg", e.Message},
	}

	if e.File != "" {
		fields = append(fields, kvPair{"caller", fmt.Sprintf("%s:%d", e.File, e.Line)})
	}

	if e.Func != "" {
		fields = append(fields, kvPair{"func", e.Func})
	}

	for _, field := range e.Fields {
		fields = append(fields, kvPair{field.Name, field.Value})
	}

	lf.mtx.Lock()
	defer lf.mtx.Unlock()

	for _, entry := range fields {
		_ = lf.writer.EncodeKeyval(entry[0], entry[1])
	}

	_ = lf.writer.EndRecord()
}

type syncer interface {
	Sync() error
}

func (lf LogfmtSink) Sync() {
	if s, ok := lf.under.(syncer); ok {
		_ = s.Sync()
	}
}
