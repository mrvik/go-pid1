# PID 1 written in Go
> Because why the hell not.

Have you ever wondered what if the init was written in go?
Well, here it is. Overall experience was pretty straightforward and didn't hit any limitation in the language.
Maybe some docs missing on some `syscall` or `golang.com/x/sys/unix` functions, but nothing that cannot be found on the `man` pages.

This is more like a proof of concept than an actual init system you want to use.
Don't get me wrong, it actually works, but sure, it's missing a lot of features related to managing processes and services.

It takes some ideas from other projects:
* A minimal init process with the bare minimum (from `sinit`, the suckless init).
* Each unit is a `yaml` file because who doesn't know `yaml`?
* Services are launched and watched by another process.
* Journal is in-memory and it's not persistent (ok, this is a bug really).
* All commands are statically linked.
* Dependency management is a hell and our approach is too basic, but works.
* Forking processes are watched using `cmd/run-in-cgroup` (see `examples/dhclient`)
* Use all the power of Go templates (see docs for `text/template` from stdlib). Applied on `examples/agetty.yml.template`.

## FAQ
Well, no one asked.

> Will this replace `systemd`, `OpenRC`, `runit`, etc?

No. The purpose isn't to "replace" something or be "another competing standard". The purpose is to show some concepts and a modular structure. Anyway it's usable and you may want it.

> As you said, it has missing features. How hard can it be to implement them?

It's worth noting that development was centered on processes more than on file systems or journal, so those things can be vastly improved w/o headaches.
See the [contributing](#contributing) section for details on how to contribute.

> Is it fast?

"Fast" is too ambiguous and no, I don't have done any tests because I won't port all systemd units. Try it yourself on a VM.
Anyway, dependency management is so simple and works like a charm on big systems (all dependencies in parallel w/o control goes brrr).

# How does it work?
It can work with initramfs and without it.

* `cmd/init` is started (after initramfs or directly from kernel).
* `cmd/init` disables `CTRL+ALT+DELETE` (kernel sends `SIGINT` to PID 1 instead of rebooting directly).
* `cmd/init` starts `cmd/service-launcher` and waits for it to exit.
* `cmd/service-launcher` does:
  * Mount all filesystems (common filesystems like `/dev` and those specified on fstab)
  * Set hostname with data from `/etc/hostname`
  * Set keymap invoking `loadkeys` with data from `/etc/vconsole.conf`
  * Read services from `/etc/yml-services` and start all of them
  * Listen on UNIX socket `/run/service-launcher.sock`
  * Does teardown on exit. Order is LIFO, first UNIX socket is removed and finally filesystems are unmounted (/ is remounted read only)

Other utilities included:
* `cmd/slc` - Stands for Service Launcher Control. It sends commands to Service Launcher via socket. You can also connect to socket via `openbsd-netcat` (provides `-U` option).
* `cmd/run-on-pid-ns` - Run a child process isolated inside a PID namespace
* `cmd/run-in-cgroup` - Run a process in a cgroup and watch it and it's child processes. It's made to follow processes that `fork(2)` and exit. It registers as `subreaper` for child processes.
* `cmd/exec-into-cgroup` - Internal utility for `cmd/run-in-cgroup` adding itself to the inherited cgroup and `exec(2)`-ing the real process after that.

Batteries included under `examples` folder.

## Compile
Run `make` and the `out` directory will contain all the compiled binaries (targets). You'll see it is using the pure Go net stack (it only uses Unix sockets so it's ok).
The `install` target uses `DESTDIR` to copy everything from `out` to `$(DESTDIR)/bin`.

# Want to try it?

Use a VM. It just works by adding service files into `/etc/yml-services/`. See the `examples` folder for reference.
You can copy the `examples` folder directly, don't worry about services for non-existing executables.

See `godoc` for `Service` type on `services` package for reference. Note all names are in lowercase on `yaml`.
There is a `man` page planned for `sl-service.yml(5)` documenting options and behavior.

To create an VM you'll need a Linux distro installed on an image and do `make install` with `DESTDIR` set to image mount point.
After that you can launch it and append to kernel `init=/init` (that's the path I have set on my tests, use the path where `make install` did put the `init` binary).

On `Arch Linux` we can do the following, maybe your distro has some sort of tool to do bootstrapping:

* You need `arch-install-scripts` for `pacstrap` tool
* Create the disk image. I prefer using `raw` images for tests, so `truncate -s 5G your.img`. 5G can be changed to the space you need (or reduce it, I don't care about space).
* Set the needed format, I'd recommend `ext4` or `xfs` to avoid complications on tests, so `mkfs.<your_fs> your.img`.
* Mount it, you'll need root privileges from now on.
* Then `tar x` the bootstrap or do `pacstrap -c <your mountpoint> base ...other tools you want like htop (so useful)...`. `pacstrap -c` uses your pacman cache so you don't need to download all packages from net making this step faster. You don't really need the kernel, will use the one on your `/boot`.
* You may have to `chroot(8)` into (use `arch-chroot(8)` on `Arch Linux`) and set the root password or make some changes (like locale, etc)

At this time you have a functional distro with the bare minimum to just work. Now let's add the Go-PID1 utilities.

* Compile all from `./cmd`. Use `make all`.
* Add compiled binaries to the image: `make DESTDIR=$YourMountpoint/usr/local/bin`. Use `local/bin` to avoid collisions (particularly the `init` command).
* Copy some units to `etc/yml-services/`. Look at the examples folder, they work (you may want to copy agetty in order to login).
* Unmount it.
* Run qemu as follows: `qemu-system-x86_84 -accel kvm -hda your.image -kernel <path to your kernel at /boot> -initrd <path to initramfs, prefer the fallback one> -append "root=/dev/sda init=/init rw" -m 512 -nic user,model=virtio-net-pci`.

You're all done.
Now you should have `getty` ready for you to login. After login you should be able to do use `slc` to send some commands via socket.

# What's next

Some features have been implemented.

* Squash bugs (should be found and reported first).
* Document the socket (read `cmd/service-launcher/main.go` for reference on commands)
* Journal rework (make it persistent).
* Implement `syslog` on other utility or check an existent utility like `syslogd`
