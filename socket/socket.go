package socket

import (
	"context"
	"errors"
	"fmt"
	"init/util"
	"io"
	"net"
	"os"
	"strings"

	"cdr.dev/slog"
)

// Errors.
var (
	ErrCannotParse = errors.New("cannot parse command")
)

// Default socket address
var SocketAddr = "/run/service-launcher.sock"

var logger = util.NewLog("socket")

type CommandHandler func(command, args string, result io.WriteCloser)

type Listener struct {
	listener       net.Listener
	commandHandler map[string]CommandHandler
}

type Command struct {
	Command string
	Args    string
}

func NewListener(ctx context.Context, commandHandlers map[string]CommandHandler) (*Listener, error) {
	_ = os.Remove(SocketAddr) // Ensure there is no socket listening there

	listener, err := net.Listen("unix", SocketAddr)
	if err != nil {
		return nil, fmt.Errorf("cannot create unix pipe: %w", err)
	}

	if err = os.Chmod(SocketAddr, 0o600); err != nil {
		return nil, fmt.Errorf("cannot set mode on pipe: %w", err)
	}

	logger.Info(ctx, "Socket listening", slog.F("addr", SocketAddr))

	return &Listener{
		listener:       listener,
		commandHandler: commandHandlers,
	}, nil
}

func (l *Listener) Listen(ctx context.Context) {
	for {
		conn, err := l.listener.Accept()
		if err != nil {
			if ctx.Err() != nil {
				return
			}

			logger.Error(ctx, fmt.Sprintf("Error accepting new connection: %s", err))

			return
		}

		go l.handleConn(ctx, conn)
	}
}

func (l *Listener) handleConn(ctx context.Context, conn net.Conn) {
	defer func() {
		if err := recover(); err != nil {
			logger.Error(ctx, "Recovered panic from command handler", slog.F("error", err))
		}
	}()

	cmd, err := readFromPipe(conn)
	if err != nil {
		fmt.Fprintf(conn, "Error parsing command: %s\n", err)
		conn.Close()

		return
	}

	handler, exists := l.commandHandler[cmd.Command]
	if !exists {
		fmt.Fprintf(conn, "Cannot find command manager for command %q\n", cmd.Command)
		conn.Close()

		return
	}

	handler(cmd.Command, cmd.Args, conn)
}

func (l *Listener) Close(ctx context.Context) {
	logger.Info(ctx, "Removing socket...")
	l.listener.Close()
	os.Remove(SocketAddr)
}

func readFromPipe(pipe io.Reader) (Command, error) {
	var buf [512]byte

	n, err := io.ReadAtLeast(pipe, buf[:], 1)
	if err != nil && !errors.Is(err, io.EOF) {
		return Command{}, fmt.Errorf("error reading from conn: %w", err)
	}

	spl := strings.SplitN(strings.TrimSuffix(string(buf[:n]), "\n"), " ", 2)
	if len(spl) < 1 || spl[0] == "" {
		return Command{}, fmt.Errorf("%w: %q", ErrCannotParse, spl)
	}

	var args string
	if len(spl) > 1 {
		args = spl[1]
	}

	return Command{
		Command: spl[0],
		Args:    args,
	}, nil
}
