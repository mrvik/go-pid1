package journal

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"init/util"
	"sync"

	"cdr.dev/slog"
)

var log = util.NewLog("journal")

type SubscriberMap struct {
	mp  map[string]chan<- Line
	mtx sync.RWMutex
}

func (jf *SubscriberMap) Send(line Line) {
	var idsToRemove []string

	jf.mtx.RLock()
	defer func() {
		if len(idsToRemove) == 0 { // It should be unlocked if idsToRemove was != 0
			jf.mtx.RUnlock()
		}
	}()

	for id, channel := range jf.mp {
		if !safeSend(channel, line) {
			idsToRemove = append(idsToRemove, id)
		}
	}

	if len(idsToRemove) == 0 {
		return
	}

	jf.mtx.RUnlock()
	jf.mtx.Lock()
	defer jf.mtx.Unlock()

	for _, id := range idsToRemove {
		delete(jf.mp, id)
	}
}

func (sub *SubscriberMap) add(channel chan<- Line) string {
	sub.mtx.RLock()
	defer sub.mtx.Unlock()

	for {
		randString := mustGenerate()
		if _, exists := sub.mp[randString]; exists {
			continue
		}

		sub.mtx.RUnlock()
		sub.mtx.Lock()

		if sub.mp == nil {
			sub.mp = make(map[string]chan<- Line)
		}

		sub.mp[randString] = channel

		return randString
	}
}

func (sub *SubscriberMap) delete(id string) {
	sub.mtx.Lock()
	defer sub.mtx.Unlock()

	delete(sub.mp, id)
}

func mustGenerate() string {
	var buf [32]byte

	if _, err := rand.Read(buf[:]); err != nil {
		panic(err)
	}

	return hex.EncodeToString(buf[:])
}

func safeSend(dest chan<- Line, thing Line) bool {
	defer func() {
		_ = recover()
	}()

	select {
	case dest <- thing:
	default:
		log.Warn(context.Background(), "Channel lost a message, ensure your subs desubscribe correctly", slog.F("channel", dest))
	}

	return true
}
