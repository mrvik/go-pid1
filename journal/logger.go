package journal

import (
	"context"

	"cdr.dev/slog"
)

func LineLogger(fields ...slog.Field) LineWriter {
	return LogLineWriter{log: log.With(fields...)}
}

type LogLineWriter struct {
	log slog.Logger
}

func (ll LogLineWriter) Write(line Line) error {
	ll.log.Log(context.Background(), slog.SinkEntry{
		Time:    line.Timestamp,
		Level:   slog.LevelInfo,
		Message: line.Content,
	})

	return nil
}
