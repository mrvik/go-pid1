package journal

import (
	"io"
	"sort"
	"strings"
	"sync"
	"time"
)

const ChannelBuffer = 500

type Journal struct {
	Out, Err *Formatter
}

type LineWriter interface {
	Write(Line) error
}

func NewJournal(service string, forwardStdout, forwardStderr LineWriter, plugins []JournalPlugin) *Journal {
	j := &Journal{
		Out: &Formatter{
			service: service,
			msgtype: "info",
			forward: forwardStdout,
			plugins: plugins,
		},
		Err: &Formatter{
			service: service,
			msgtype: "error",
			forward: forwardStderr,
			plugins: plugins,
		},
	}

	for _, plugin := range plugins {
		if plugin.AsyncNewJournal != nil {
			go plugin.AsyncNewJournal(j)
		}
	}

	return j
}

type Formatter struct {
	service string
	msgtype string

	plugins []JournalPlugin

	lines          Lines
	unfinishedLine string

	forward LineWriter

	mtx sync.RWMutex

	liveSubscribers SubscriberMap
}

func (f *Formatter) ServiceName() string {
	return f.service
}

// Lines are a sortable slice of lines.
type Lines []Line

func (l Lines) Len() int {
	return len(l)
}

func (l Lines) Less(i1, i2 int) bool {
	return l[i1].Timestamp.Before(l[i2].Timestamp)
}

func (l Lines) Swap(i1, i2 int) {
	l[i1], l[i2] = l[i2], l[i1]
}

// A line holds the insertion timestamp and log content (doesn't end on \n).
type Line struct {
	Content   string
	Timestamp time.Time
}

func NewLine(content string) Line {
	return Line{
		Content:   content,
		Timestamp: time.Now(),
	}
}

func (l Line) Format() string {
	return l.Timestamp.Format(time.RFC3339) + " " + l.Content + "\n"
}

// Implement io.Writer. Safe for concurrent use.
func (jf *Formatter) Write(bytes []byte) (int, error) {
	if len(bytes) == 0 {
		return 0, nil
	}

	var lineBuilder strings.Builder

	jf.mtx.Lock()
	defer jf.mtx.Unlock()

	if jf.unfinishedLine == "" {
		lineBuilder.WriteString(jf.msgtype + " " + jf.service + " ")
	} else {
		lineBuilder.WriteString(jf.unfinishedLine)
	}

	for _, b := range bytes {
		if b == '\n' {
			line := NewLine(lineBuilder.String())

			if !jf.filterLineWithPlugins(line) {
				lineBuilder.Reset()

				continue
			}

			jf.lines = append(jf.lines, line)
			jf.liveSubscribers.Send(line)

			if jf.forward != nil {
				jf.forward.Write(line)
			}

			lineBuilder.Reset()
			lineBuilder.WriteString(jf.msgtype + " " + jf.service + " ")
		} else {
			lineBuilder.WriteByte(b)
		}
	}

	jf.unfinishedLine = lineBuilder.String()

	return len(bytes), nil
}

func (jf *Formatter) filterLineWithPlugins(l Line) bool {
	for _, plugin := range jf.plugins {
		if plugin.FilterLine != nil && !plugin.FilterLine(l) {
			return false
		}
	}

	return true
}

func (j *Journal) WriteTo(w io.Writer) (n int64, err error) {
	j.Out.mtx.RLock()
	defer j.Out.mtx.RUnlock()
	j.Err.mtx.RLock()
	defer j.Err.mtx.RUnlock()
	lines := append(j.Out.lines, j.Err.lines...)
	sort.Sort(lines)

	for _, line := range lines {
		var ac int
		ac, err = w.Write([]byte(line.Format()))
		n += int64(ac)

		if err != nil {
			break
		}
	}

	return
}

func (jf *Formatter) subscribe(channel chan<- Line) string {
	return jf.liveSubscribers.add(channel)
}

func (jf *Formatter) deleteSubscription(id string) {
	jf.liveSubscribers.delete(id)
}

// Return a channel that receives all new lines from the selected outputs. Caller should call DeleteSubscription to close channel.
func (j *Journal) SubscribeTo(out, err bool) ([2]string, <-chan Line) {
	var (
		ids [2]string
		ch  = make(chan Line, ChannelBuffer)
	)

	if !(out || err) {
		close(ch) // Return a closed channel

		return ids, ch
	}

	if out {
		ids[0] = j.Out.subscribe(ch)
	}

	if err {
		ids[1] = j.Err.subscribe(ch)
	}

	return ids, ch
}

// Delete subscription from channel
func (j *Journal) DeleteSubscription(ids [2]string) {
	if ids[0] != "" {
		j.Out.deleteSubscription(ids[0])
	}

	if ids[1] != "" {
		j.Err.deleteSubscription(ids[1])
	}
}
