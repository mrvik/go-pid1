package journal

type JournalPlugin struct {
	// Return true / false in order to keep or discard a journal line.
	FilterLine func(Line) bool
	// Ran in a separate goroutine, this function is called when a new Journal is created.
	AsyncNewJournal func(*Journal)
}
