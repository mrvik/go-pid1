package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

const batchTickDuration = time.Second * 10

type LineReader interface {
	HandleLine(FormattedLine) error
}

type SingleHandler string

func (url SingleHandler) HandleLine(line FormattedLine) error {
	return postJSON(string(url), line)
}

func postJSON(to string, content interface{}) error {
	reader := jsonReader(content)
	defer reader.Close()

	res, err := http.Post(to, "application/json", reader)
	switch {
	case err != nil:
		return fmt.Errorf("post log to %q: %w", to, err)
	case res.StatusCode > 299:
		defer res.Body.Close()
		return fmt.Errorf("%w: server responded w/ status %d (%s)", ErrServerError, res.StatusCode, res.Status)
	}

	res.Body.Close()

	return nil
}

func jsonReader(from interface{}) *io.PipeReader {
	read, write := io.Pipe()
	go func() {
		defer write.Close()
		_ = json.NewEncoder(write).Encode(from)
	}()

	return read
}

type BatchHandler struct {
	batch   int
	target  string
	channel chan FormattedLine
}

func NewBatchHandler(size int, target string) *BatchHandler {
	h := &BatchHandler{
		batch:   size,
		target:  target,
		channel: make(chan FormattedLine, 2),
	}

	go h.worker()

	return h
}

func (bh BatchHandler) HandleLine(line FormattedLine) error {
	bh.channel <- line

	return nil
}

func (bh BatchHandler) worker() {
	ticker := time.NewTicker(batchTickDuration)

	for {
		lines := bh.waitLines(ticker.C)

		if len(lines) > 0 {
			bh.sendBatch(lines)
		}

		ticker.Reset(batchTickDuration)
	}
}

func (bh BatchHandler) waitLines(ticker <-chan time.Time) []FormattedLine {
	lines := make([]FormattedLine, 0, bh.batch)

	for {
		select {
		case line := <-bh.channel:
			lines = append(lines, line)
		case <-ticker:
			return lines
		}

		if len(lines) >= bh.batch {
			return lines
		}
	}
}

func (b BatchHandler) sendBatch(lines []FormattedLine) {
	if err := postJSON(b.target, BatchFormattedLine{lines}); err != nil {
		log.Error(context.Background(), fmt.Sprintf("Post %d batch logs: %s", len(lines), err))
	}
}
