package main

import (
	"errors"
	"init/journal"
	"init/plugins"
	"init/util"
	"os"
	"strconv"
	"time"
)

var (
	url          = os.Getenv("JOURNAL_URL")
	batchSize, _ = strconv.Atoi(os.Getenv("JOURNAL_BATCH_SIZE"))
	log          = util.NewLog("plugin:journal-poster")
)

var Register = plugins.PluginRegisterSymbol(_Register)

var (
	ErrServerError = errors.New("server responded with error")
	ErrNoURL       = errors.New("no URL to send logs to")
)

func RegisterPost(j *journal.Journal) {
	_, ch := j.SubscribeTo(true, true)
	svcName := j.Out.ServiceName()
	hostname, _ := os.Hostname()

	var handler LineReader

	if batchSize > 1 {
		handler = NewBatchHandler(batchSize, url)
	} else {
		handler = SingleHandler(url)
	}

	go func() {
		for line := range ch {
			handler.HandleLine(FormatLine(line, svcName, hostname))
		}
	}()
}

type BatchFormattedLine struct {
	Batch []FormattedLine `json:"batch"`
}

type FormattedLine struct {
	Message  string                `json:"message"`
	Metadata FormattedLineMetadata `json:"metadata"`
}

type FormattedLineMetadata struct {
	Timestamp time.Time `json:"timestamp"`
	Service   string    `json:"service"`
	Hostname  string    `json:"hostname"`
}

func FormatLine(line journal.Line, service, hostname string) FormattedLine {
	return FormattedLine{
		Message: line.Content,
		Metadata: FormattedLineMetadata{
			Timestamp: line.Timestamp,
			Service:   service,
			Hostname:  hostname,
		},
	}
}

func _Register(into *plugins.PluginList) error {
	if url == "" {
		return ErrNoURL
	}

	into.JournalPlugins = append(into.JournalPlugins, journal.JournalPlugin{
		AsyncNewJournal: RegisterPost,
	})

	return nil
}
