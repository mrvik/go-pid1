package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"

	"github.com/MicahParks/keyfunc"
	"github.com/golang-jwt/jwt/v4"
)

var ErrBadStatus = errors.New("bad status on server response")

type Authentication struct {
	OpenID OIDCConfig
}

func (a Authentication) Middleware(to http.Handler) (http.Handler, error) {
	return NewOIDCMiddleware(a.OpenID, to)
}

type OIDCConfig struct {
	Audience     string
	Issuer       string
	StaticConfig *OpenIDConfig `yaml:"static-config"`
}

func NewOIDCMiddleware(conf OIDCConfig, next http.Handler) (*OIDCMiddleware, error) {
	var oidc OpenIDConfig

	if conf.StaticConfig != nil {
		oidc = *conf.StaticConfig
	} else {
		cnf, err := getOpenIDConfig(conf.Issuer)
		if err != nil {
			return nil, err
		}

		oidc = *cnf
	}

	kf, err := keyfunc.Get(oidc.JWKsURI, keyfunc.Options{
		RefreshRateLimit:  1 * time.Minute,
		RefreshUnknownKID: true,
	})
	if err != nil {
		return nil, fmt.Errorf("create key checker: %w", err)
	}

	return &OIDCMiddleware{
		next: next,
		aud:  conf.Audience,
		iss:  conf.Issuer,
		kfn:  kf.Keyfunc,
	}, nil
}

type OpenIDConfig struct {
	Issuer  string `json:"string"`
	JWKsURI string `json:"jwks_uri"`
}

func getOpenIDConfig(issuer string) (*OpenIDConfig, error) {
	target, err := url.Parse(issuer)
	if err != nil {
		return nil, fmt.Errorf("parse issuer URL: %w", err)
	}

	target.Path = path.Join(target.Path, ".well-known/openid-configuration")

	res, err := http.Get(target.String())
	if err != nil {
		return nil, fmt.Errorf("make oidc config query: %w", err)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%w: unexpected status %s", ErrBadStatus, res.Status)
	}

	configTarget := new(OpenIDConfig)

	if err := json.NewDecoder(res.Body).Decode(configTarget); err != nil {
		return nil, fmt.Errorf("decode openid config: %w", err)
	}

	return configTarget, nil
}

type OIDCMiddleware struct {
	next http.Handler
	aud  string
	iss  string
	kfn  jwt.Keyfunc
}

var unauthorizedMessage = struct {
	Message string `json:"message"`
}{
	Message: "Unauthorized request",
}

func badAuth(rw http.ResponseWriter) {
	rw.WriteHeader(http.StatusUnauthorized)
	json.NewEncoder(rw).Encode(unauthorizedMessage)
}

type claimsVerifier interface {
	VerifyAudience(string, bool) bool
	VerifyIssuer(string, bool) bool
}

func (o OIDCMiddleware) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	tokenString := strings.TrimPrefix(req.Header.Get("Authorization"), "Bearer ")
	if tokenString == "" {
		badAuth(rw)

		return
	}

	token, err := jwt.ParseWithClaims(tokenString, &jwt.RegisteredClaims{}, o.kfn)
	if err != nil {
		badAuth(rw)

		return
	}

	claims := token.Claims.(claimsVerifier)

	if !claims.VerifyAudience(o.aud, true) || !claims.VerifyIssuer(o.iss, true) {
		badAuth(rw)

		return
	}

	o.next.ServeHTTP(rw, req)
}
