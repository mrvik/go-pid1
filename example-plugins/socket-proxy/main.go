package main

import (
	"context"
	"errors"
	"fmt"
	"init/plugins"
	"init/util"
	"net"
	"net/http"
	"os"
	"time"

	"cdr.dev/slog"
	"gopkg.in/yaml.v3"
)

var log = util.NewLog("plugin:socket-proxy").With(slog.F("plugin", "socket-proxy"))

var Register = plugins.PluginRegisterSymbol(_Register)

func _Register(into *plugins.PluginList) error {
	plugin, err := NewProxy(context.Background())
	if err != nil {
		return fmt.Errorf("create socket proxy: %w", err)
	}

	into.ServicePlugins = append(into.ServicePlugins, plugin)

	return nil
}

type Config struct {
	Services map[string]BackendConfig
	Addr     string
}

type BackendConfig struct {
	Socket   string
	Path     string
	TrimPath bool `yaml:"trim-path"`
	Auth     *Authentication
}

type Proxy struct {
	Services map[string]BackendConfig
	server   *http.Server
}

func NewProxy(ctx context.Context) (*Proxy, error) {
	config, err := readConfig()
	if err != nil {
		return nil, err
	}

	clearSockets(config.Services)

	proxy := &Proxy{
		Services: config.Services,
		server: &http.Server{
			ReadHeaderTimeout: time.Second * 10,
		},
	}

	return proxy, proxy.startServer(ctx, config)
}

func clearSockets(services map[string]BackendConfig) error {
	for _, svc := range services {
		socket := svc.Socket

		if err := os.Remove(socket); err != nil && !os.IsNotExist(err) {
			return fmt.Errorf("remove socket at %q: %w", socket, err)
		}
	}

	return nil
}

func (p *Proxy) startServer(ctx context.Context, config Config) error {
	laddr, err := net.ResolveTCPAddr("tcp", config.Addr)
	if err != nil {
		return fmt.Errorf("parse listen address %q: %w", config.Addr, err)
	}

	listener, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		return fmt.Errorf("start listening on %q: %w", laddr.String(), err)
	}

	go func() {
		p.server.Handler = configureServer(ctx, config)

		log.Info(ctx, "Accepting connections", slog.F("addr", listener.Addr().String()))

		if err := p.server.Serve(listener); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Error(ctx, "server crashed", slog.Error(err))
		}
	}()

	return nil
}

func readConfig() (Config, error) {
	filename := defaultString(os.Getenv("SOCKET_PROXY_CONFIG"), "/etc/socket-proxy.yaml")

	file, err := os.Open(filename)
	if err != nil {
		return Config{}, fmt.Errorf("read config file: %w", err)
	}

	defer file.Close()

	var config Config

	if err := yaml.NewDecoder(file).Decode(&config); err != nil {
		return Config{}, fmt.Errorf("decode config file at %q: %w", filename, err)
	}

	return config, nil
}

func defaultString(a, b string) string {
	if a == "" {
		return b
	}

	return a
}

func (p Proxy) SupervisorStarted() {
	// NOOP
}

func (p Proxy) NotifyStart(serviceName string) {
	// NOOP
}

func (p Proxy) NotifyDeath(serviceName string) {
	service, registered := p.Services[serviceName]
	if !registered {
		return
	}

	log := log.With(slog.F("service", serviceName))
	ctx := context.Background()

	if err := os.Remove(service.Socket); err != nil && !os.IsNotExist(err) {
		log.Warn(ctx, "Can't remove leftover socket", slog.Error(err))
	}

	log.Info(ctx, "Removed leftover socket")
}

func (p Proxy) SupervisorStopping() {
	defer p.server.Close()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5) //nolint: gomnd
	defer cancel()

	log.Info(ctx, "Stopping server")

	_ = p.server.Shutdown(ctx)
}
