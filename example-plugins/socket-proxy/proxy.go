package main

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httputil"
	"net/url"
	"path"
	"strings"

	"cdr.dev/slog"
	"github.com/tv42/httpunix"
)

func configureServer(ctx context.Context, config Config) *http.ServeMux {
	mux := http.NewServeMux()
	rootRegistered := false

	for name, backend := range config.Services {
		lg := log.With(slog.F("backend", name))

		servePath := path.Clean("/" + backend.Path)
		if !strings.HasSuffix(servePath, "/") {
			servePath += "/"
		}

		if servePath == "/" {
			rootRegistered = true
		}

		transport := &httpunix.Transport{}
		url := url.URL{
			Scheme: httpunix.Scheme,
			Host:   "socket",
		}

		transport.RegisterLocation("socket", backend.Socket)
		lg.Info(ctx, "registering backend", slog.F("from", backend.Socket), slog.F("to", servePath))

		proxy := httputil.NewSingleHostReverseProxy(&url)
		proxy.Transport = transport
		proxy.ErrorLog = slog.Stdlib(ctx, lg, slog.LevelError)
		handler := http.Handler(proxy)

		if backend.TrimPath {
			handler = http.StripPrefix(servePath, proxy)
		}

		if backend.Auth != nil {
			lg.Info(ctx, "Creating auth middleware")

			middle, err := backend.Auth.Middleware(handler)
			if err != nil {
				lg.Error(ctx, "Create auth middleware", slog.Error(err))

				middle = http.HandlerFunc(func(rw http.ResponseWriter, _ *http.Request) {
					http.Error(rw, "Cannot create security middleware. Endpoint disabled", http.StatusServiceUnavailable)
				})
			}

			handler = middle
		}

		mux.Handle(servePath, handler)
	}

	if !rootRegistered {
		mux.Handle("/", DefaultHandler(config))
	}

	return mux
}

type DefaultHandler Config

func (c DefaultHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Set("Content-Type", "application/json")

	_ = json.NewEncoder(rw).Encode(c)
}
