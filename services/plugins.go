package services

func (s *Service) notifyStarted() {
	for _, p := range s.servicePlugins {
		p.NotifyStart(s.Name)
	}
}

func (s *Service) notifyStopped() {
	for _, p := range s.servicePlugins {
		p.NotifyDeath(s.Name)
	}
}

func (sm *ServiceManager) notifyInit() {
	for _, p := range sm.pluginList.ServicePlugins {
		p.SupervisorStarted()
	}
}

func (sm *ServiceManager) notifyStop() {
	for _, p := range sm.pluginList.ServicePlugins {
		p.SupervisorStopping()
	}
}
