package services

import (
	"context"
	"fmt"
	"time"

	"cdr.dev/slog"
)

// Reloading services is hard, we have to make a diff between the actual services and the new specs

// Read all units and compare with the actual ones
func (sm *ServiceManager) Reload(ctx context.Context) error {
	services, allNewMapped, err := readServices(ctx, sm.pluginList.ServicePlugins)
	if err != nil {
		return err
	}

	// Counters
	var (
		removed, added, updated int
	)

	// Remove services to be removed from map and slice
	slice := sm.services
	for index, service := range slice {
		if _, exists := allNewMapped[service.Name]; !exists {
			tctx, cancel := context.WithTimeout(ctx, time.Second*10)
			if err := service.Stop(tctx); err != nil {
				logger.Error(ctx, fmt.Sprintf("Error stopping deleted service: %s", err), slog.F("service", service.Name))
			}
			cancel()
			slice[index] = nil
			delete(sm.mappedServices, service.Name)
			removed++
		}
	}

	// Fix slice removing all nil services
	sm.services = make([]*Service, 0, len(slice))
	for _, svc := range slice {
		if svc != nil {
			sm.services = append(sm.services, svc)
		}
	}

	// Now we add new services
	for _, service := range services {
		if _, exists := sm.mappedServices[service.Name]; !exists {
			sm.services = append(sm.services, service)
			sm.mappedServices[service.Name] = service
			added++
		}
	}

	// Update current services needing it
	for _, service := range services {
		var (
			// All services must exist in map (new have been added and current are already in)
			currentService           = sm.mappedServices[service.Name]
			updateFields, updateDeps = currentService.Diff(service)
			newDeps                  []*Service
			log                      = logger.With(slog.F("service", currentService.Name))
		)

		switch {
		case updateDeps:
			var (
				notFound []string
				cyclic   bool
			)
			newDeps, notFound, cyclic = getDependencies(allNewMapped, service.Dependencies)
			if cyclic {
				log.Error(ctx, "Cyclic dependency detected while reloading service")

				continue
			} else if len(notFound) == 0 {
				log.Error(ctx, "Missing dependencies while reloading service", slog.F("missing", notFound))

				continue
			}

			if newDeps == nil {
				newDeps = make([]*Service, 0) // Make it non-nil (removed deps from service)
			}

			fallthrough
		case updateFields:
			currentService.Update(service, newDeps)
			updated++
		default:
			// Do nothing
		}
	}

	logger.Info(ctx, "Reload summary",
		slog.F("added", added),
		slog.F("removed", removed),
		slog.F("updated-services", updated),
		slog.F("current-services", len(sm.services)),
	)

	return sm.StartAll(ctx)
}

// Check if fields have been updated on service. If updateDeps is true, updateFields will also be true.
// If updateFields is false, updateFields may be true or false.
// When a deps update is needed, the whole service needs to be updated.
func (current *Service) Diff(update *Service) (updateFields, updateDeps bool) {
	current.serviceMutex.RLock()
	defer current.serviceMutex.RUnlock()
	updateDeps = diffStringSlice(current.Dependencies, update.Dependencies)
	updateFields = updateDeps ||
		current.Description != update.Description ||
		current.Exec != update.Exec ||
		diffStringSlice(current.Arguments, update.Arguments) ||
		diffStringSlice(current.Environment, update.Environment) ||
		current.Restart != update.Restart
	return
}

func (current *Service) Update(update *Service, newDeps []*Service) {
	current.serviceMutex.Lock()
	defer current.serviceMutex.Unlock()

	current.Description, current.Exec, current.Restart,
		current.Arguments, current.Environment =
		update.Description, update.Exec, update.Restart,
		update.Arguments, update.Environment

	if newDeps == nil { // Not checking length, only if nil. May be a slice of len==0 to remove deps
		return
	}

	current.Dependencies = update.Dependencies
	current.foundDependencies = newDeps
}

func diffStringSlice(s1, s2 []string) bool {
	if len(s1) != len(s2) {
		return true
	}

	for i, v := range s1 {
		if s2[i] != v {
			return true
		}
	}

	return false
}
