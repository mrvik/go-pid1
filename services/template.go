package services

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
)

const ArgumentSeparator = '@'

func SplitArgument(filename string) (realName, argument string) {
	name := strings.TrimSuffix(strings.TrimSuffix(filename, ".yml"), ".yaml")

	if !strings.ContainsRune(name, ArgumentSeparator) {
		return name, ""
	}

	sepIndex := strings.LastIndex(name, string(ArgumentSeparator))
	realName, argument = name[:sepIndex], name[sepIndex+1:] // +1 to trim @

	return
}

func ApplyTemplate(reader io.Reader, filename, argument string) (*bytes.Buffer, error) {
	content, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, fmt.Errorf("cannot read template from %s: %w", filename, err)
	}

	var (
		data = struct {
			Argument string
		}{
			argument,
		}
		funcmap = template.FuncMap{
			"Getenv": os.Getenv,
		}
		buffer bytes.Buffer
	)

	tmpl, err := template.New(filename).Funcs(funcmap).Parse(string(content))
	if err != nil {
		return nil, fmt.Errorf("cannot parse file %q as a template: %w", filename, err)
	}

	if err = tmpl.Funcs(funcmap).Execute(&buffer, data); err != nil {
		return nil, fmt.Errorf("cannot execute template %q: %w", tmpl.Name(), err)
	}

	return &buffer, nil
}
