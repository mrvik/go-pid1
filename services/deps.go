package services

import (
	"context"
	"time"
)

const (
	NotFoundTimeout = time.Second
)

func getDependencies(haystack map[string]*Service, needles []string) (deps []*Service, notFound []string, cyclic bool) {
	if len(needles) == 0 {
		return
	}

	for _, needle := range needles {
		var (
			svc    *Service
			exists bool
		)
		if svc, exists = haystack[needle]; !exists {
			notFound = append(notFound, needle)
			continue
		}

		for _, dep := range svc.Dependencies {
			if dep == needle {
				cyclic = true
				return
			}
		}

		deps = append(deps, svc)
	}

	return
}

// Wait for all services on the slice to be started.
// Will fail if any of them fails (bad config or failed process).
// If context expires, error will be context.Err().
func waitForServices(ctx context.Context, services []*Service) error {
	if len(services) == 0 {
		return nil
	}

	if err := ctx.Err(); err != nil {
		return err
	}

	ready := make(chan error, len(services))
	for _, service := range services {
		go func(s *Service) {
			ready <- s.waitStart(ctx)
		}(service)
	}

	for range services {
		select {
		case err := <-ready:
			if err == nil {
				continue
			}
			return err
		case <-ctx.Done():
			return ctx.Err()
		}
	}

	return ctx.Err() // Just to be sure
}
