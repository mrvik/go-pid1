package services

import (
	"context"
	"errors"
	"fmt"
	"init/journal"
	"init/plugins"
	"runtime"
	"sync"
	"time"

	"cdr.dev/slog"
	"golang.org/x/sync/semaphore"
)

func (sm *ServiceManager) StartAll(ctx context.Context) error {
	var (
		smp          = semaphore.NewWeighted(int64(runtime.NumCPU()))
		wg           sync.WaitGroup
		serviceNames = make(map[string]*Service)
	)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, service := range sm.services {
			serviceNames[service.Name] = service
		}
	}()

	// First we start services w/o dependencies
	for _, service := range sm.services {
		if len(service.Dependencies) != 1 {
			continue
		}

		if err := smp.Acquire(ctx, 1); err != nil {
			return err
		}

		wg.Add(1)

		go func(service *Service) {
			defer smp.Release(1)
			defer wg.Done()

			err := service.Start(ctx, GenerateJournal(service, sm.pluginList))
			if err != nil {
				logger.Error(ctx, fmt.Sprintf("Error starting service w/o dependencies: %s", err), slog.F("service", service.Name))
			}
		}(service)
	}

	wg.Wait()

	// Create another one
	wg = sync.WaitGroup{}

	// Now we start processes w/ dependencies
	// We're not using the semaphore bc most goroutines will be blocked waiting and not doing actual work
	for _, service := range sm.services {
		// Service was already started
		if !errors.Is(service.State.Err(), ErrNotStarted) {
			continue
		}

		wg.Add(1)
		go func(service *Service) {
			defer wg.Done()

			log := logger.With(slog.F("service", service.Name))

			deps, notFound, cyclic := getDependencies(serviceNames, service.Dependencies)
			service.foundDependencies = deps
			if cyclic {
				log.Error(ctx, "Cyclic dependency found on service", slog.F("dependencies", service.Dependencies))
				service.State.ErrorBadConfig()

				return
			} else if len(notFound) != 0 {
				log.Error(ctx, "Missing dependencies for service", slog.F("missing", notFound), slog.F("all-dependencies", service.Dependencies))
				service.State.ErrorBadConfig()

				return
			}

			if err := service.Start(ctx, GenerateJournal(service, sm.pluginList)); err != nil {
				log.Error(ctx, fmt.Sprintf("Error starting service: %s", err))
			}
		}(service)
	}

	wg.Wait()

	return nil
}

func GenerateJournal(service *Service, pluginList plugins.PluginList) *journal.Journal {
	var (
		stdout, stderr journal.LineWriter
		serviceField   = slog.F("service", service.Name)
	)

	if service.ForwardJournal {
		stdout = journal.LineLogger(slog.F("output", "stdout"), serviceField)
		stderr = journal.LineLogger(slog.F("output", "stderr"), serviceField)
	}

	return journal.NewJournal(service.Name, stdout, stderr, pluginList.JournalPlugins)
}

func (sm *ServiceManager) StopAll(ctx context.Context) {
	var wg sync.WaitGroup

	sm.notifyStop()

	for _, service := range sm.services {
		if !service.IsProcessAlive() {
			continue
		}

		wg.Add(1)

		log := logger.With(slog.F("service", service.Name))
		tctx, cancel := context.WithTimeout(ctx, time.Second*60)
		defer cancel()

		go func(ctx context.Context, service *Service) {
			defer wg.Done()

			log.Info(ctx, "Stopping service")

			if err := service.Stop(ctx); err != nil {
				log.Error(ctx, fmt.Sprintf("Error stopping service: %s", err))
			}
		}(tctx, service)
	}

	wg.Wait()
}
