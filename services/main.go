// Package services implements logic to read and parse service units
// and running services. Monitoring is bloat. Just kidding, it's too hard ftm
package services

import (
	"context"
	"errors"
	"fmt"
	"init/plugins"
	"init/util"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"cdr.dev/slog"
	"golang.org/x/sync/errgroup"
	yaml "gopkg.in/yaml.v3"
)

// Dir where services will be looked
var ServicesDir = "/etc/yml-services/"

// Errors.
var (
	ErrDuplicatedName = errors.New("duplicate service")
)

var logger = util.NewLog("services")

type ServiceManager struct {
	ctx                   context.Context
	services              []*Service
	mappedServices        map[string]*Service
	deadChildrenBroadcast []chan<- bool
	pluginList            plugins.PluginList
}

// Create a new ServiceManager and read all service files
func NewServiceManager(ctx context.Context, incomingDeadChildren <-chan bool, plugins plugins.PluginList) (*ServiceManager, error) {
	services, mappedServices, err := readServices(ctx, plugins.ServicePlugins)
	if err != nil {
		return nil, err
	}

	sm := &ServiceManager{
		ctx:            ctx,
		services:       services,
		mappedServices: mappedServices,
		pluginList:     plugins,
	}

	sm.prepareChannels(incomingDeadChildren)
	sm.notifyInit()

	return sm, nil
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}

func readServices(ectx context.Context, plugins []plugins.ServicePlugin) (services []*Service, mappedServices map[string]*Service, err error) {
	ctx, cancel := context.WithCancel(ectx)
	defer cancel()

	fileList, err := ioutil.ReadDir(ServicesDir)
	if err != nil {
		err = fmt.Errorf("read services: %w", err)
		return
	}

	group, gctx := errgroup.WithContext(ctx)
	group.SetLimit(max(1, runtime.NumCPU()))

	mappedServices = make(map[string]*Service)
	mtx := new(sync.Mutex)

	for _, file := range fileList {
		if file.IsDir() {
			continue
		}

		name := file.Name()
		if !(strings.HasSuffix(name, ".yml") || strings.HasSuffix(name, ".yaml")) {
			continue
		}

		group.Go(func() error {
			if err := gctx.Err(); err != nil {
				return err
			}

			pth := path.Join(ServicesDir, name)
			log := logger.With(slog.F("file", name), slog.F("path", pth))

			file, err := os.Open(pth)
			if err != nil {
				log.Warn(ctx, fmt.Sprintf("[SKIP-PRE] Can't load services: %s", err))

				return nil
			}
			defer file.Close()

			log.Info(ctx, "Loading services")

			svcs, err := parseService(file)
			if err != nil {
				log.Warn(ctx, fmt.Sprintf("[SKIP] Parse service file: %s", err))

				return nil
			}

			if len(svcs) == 0 {
				log.Warn(ctx, "[SKIP] No services defined in file")

				return nil
			}

			for _, svc := range svcs {
				if err := gctx.Err(); err != nil {
					return err
				}

				log := log.With(slog.F("service", svc.Name))

				if err := svc.Validate(); err != nil {
					log.Error(ctx, fmt.Sprintf("[SKIP-POST] Error validating service: %s", err))

					return nil
				}

				svc.servicePlugins = plugins

				func() {
					log.Info(ctx, "Loading service")

					mtx.Lock()
					defer mtx.Unlock()

					// Check for duplicate services
					if _, e := mappedServices[svc.Name]; e {
						log.Error(ctx, "[SKIP] Duplicate service with the same name")

						return
					}

					services = append(services, svc)
					mappedServices[svc.Name] = svc
				}()
			}

			return nil
		})
	}

	if err = group.Wait(); err != nil {
		err = fmt.Errorf("read services: %w", err)
	}

	return
}

func (sm *ServiceManager) prepareChannels(incomingDeadChildren <-chan bool) {
	for _, svc := range sm.services {
		dcc := make(chan bool, 2)
		svc.deadChildren = dcc
		sm.deadChildrenBroadcast = append(sm.deadChildrenBroadcast, dcc)
	}

	go sm.listenDeadChild(incomingDeadChildren)
}

func (sm *ServiceManager) listenDeadChild(channel <-chan bool) {
	for b := range channel {
		for _, ch := range sm.deadChildrenBroadcast {
			select {
			case ch <- b:
			default: // Don't block here
				logger.Info(sm.ctx, fmt.Sprintf("Channel %p is losing messages\n", ch))
			}
		}
	}
}

func parseService(file *os.File) ([]*Service, error) {
	var (
		from        = io.Reader(file)
		_, argument = SplitArgument(filepath.Base(file.Name()))
		err         error
	)
	if argument != "" {
		if from, err = ApplyTemplate(file, file.Name(), argument); err != nil {
			return nil, err
		}
	}

	svcs := make([]*Service, 0, 1)
	decoder := yaml.NewDecoder(from)
	decoder.KnownFields(true)
	return svcs, decoder.Decode(&svcs)
}
