package services

import (
	"syscall"
)

// Restart policies have this type.
type RestartPolicy string

// Possible policies.
const (
	// Always restart on exit.
	RestartAlways = "always"
	// Restart only if failed (exit!=0).
	RestartFailed = "failed"
	// Never restart it.
	RestartNever = "never"
)

func (rp RestartPolicy) FromStatus(exit syscall.WaitStatus) bool {
	return rp == RestartAlways || (rp == RestartFailed && exit.ExitStatus() != 0)
}
