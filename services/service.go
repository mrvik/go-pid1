package services

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"
	"syscall"
	"time"

	"init/journal"
	"init/plugins"

	"cdr.dev/slog"
)

// Validation errors.
var (
	ErrNoName     = errors.New("no name specified on service")
	ErrNoExec     = errors.New("no exec specified on service")
	ErrBadRestart = errors.New("bad restart policy")

	// State errors (from ServiceState.Err())
	ErrNotStarted = errors.New("service not started")
	ErrFailed     = errors.New("service failed to start")
	ErrBadConfig  = errors.New("bad service config")
)

type Service struct {
	Name        string
	Description string

	// Exec may be a path or a simple name. Will search for those names on the PATH
	Exec        string
	Arguments   []string
	Environment []string
	Dir         string
	KillSignal  syscall.Signal `yaml:"kill-signal"`

	// Forward journal to stdout/err
	ForwardJournal bool `yaml:"forward-journal"`

	// Ensure dir exists before launching command
	// with the specified mode
	EnsureDir *struct {
		Path string
		Mode os.FileMode
	} `yaml:"ensure-dir"`

	// One restart policy from "always" "failed" or "never".
	// If empty, "never" will be useas as default.
	Restart RestartPolicy

	// Services to be started before this one
	Dependencies []string

	State   ServiceState     `yaml:"-"`
	Process *os.Process      `yaml:"-"`
	Journal *journal.Journal `yaml:"-"`

	// Dependencies are found and mapped here:
	foundDependencies []*Service

	serviceMutex sync.RWMutex
	watchCtx     context.Context
	cancelWatch  context.CancelFunc
	deadChildren <-chan bool

	servicePlugins []plugins.ServicePlugin
}

func (s *Service) Validate() error {
	if s.Name == "" {
		return ErrNoName
	}

	if s.Exec == "" {
		return ErrNoExec
	}

	switch s.Restart {
	case "", RestartAlways, RestartFailed, RestartNever:
	default:
		return fmt.Errorf("%w: %s", ErrBadRestart, s.Restart)
	}

	return nil
}

func (s *Service) Start(ctx context.Context, journal *journal.Journal) error {
	if s.Process != nil {
		if s.IsProcessAlive() {
			return nil
		}
	}

	if s.EnsureDir != nil {
		if err := os.MkdirAll(s.EnsureDir.Path, s.EnsureDir.Mode); err != nil {
			return err
		}
		if err := os.Chmod(s.EnsureDir.Path, s.EnsureDir.Mode); err != nil {
			return err
		}
	}

	if journal != nil {
		s.Journal = journal
	}

	path, err := exec.LookPath(s.Exec)
	if err != nil {
		err = fmt.Errorf("cannot find exec for %s: %w", s.Name, err)
		s.Journal.Err.Write(append([]byte(err.Error()), '\n'))
		return err
	}

	cmd := exec.Cmd{
		Path:   path,
		Args:   append([]string{path}, s.Arguments...),
		Env:    append(os.Environ(), s.Environment...),
		Stdin:  nil,
		Stdout: s.Journal.Out,
		Stderr: s.Journal.Err,
		Dir:    s.Dir,
	}

	err = waitForServices(ctx, s.foundDependencies)

	s.State.mtx.Lock()
	defer s.State.mtx.Unlock()

	if err != nil { // Error from waitForServices
		s.State.Failed = true
		return err
	}

	if err = cmd.Start(); err != nil {
		s.State.Failed = true

		return fmt.Errorf("cannot start service %s: %w", s.Name, err)
	}

	s.notifyStarted()

	s.serviceMutex.Lock()
	defer s.serviceMutex.Unlock()
	s.State.Started = true
	s.State.Failed = false // ATM
	s.Process = cmd.Process

	if s.watchCtx == nil || s.watchCtx.Err() != nil {
		s.watchCtx, s.cancelWatch = context.WithCancel(ctx)
	}
	go s.watchIt(ctx)

	return nil
}

func (s *Service) watchIt(ctx context.Context) {
	log := s.log()

	for {
		select {
		case <-s.watchCtx.Done():
			return
		case <-s.deadChildren:
			// A child process has dead, look if it was ours
		}
		var exit syscall.WaitStatus

		pid, err := syscall.Wait4(s.Process.Pid, &exit, syscall.WNOHANG, nil)
		if pid == 0 {
			continue // Not ours, nothing to see here
		}
		if err != nil {
			log.Info(ctx, fmt.Sprintf("Error waiting for process: %s", err))

			return
		}

		if exit.Signaled() {
			log.Info(ctx, "Process stopped", slog.F("signal", exit.Signal().String()))
		}

		log.Info(ctx, "Process exited", slog.F("status", exit.ExitStatus()))

		s.State.mtx.Lock()
		s.State.Started = false
		s.State.Failed = exit.ExitStatus() != 0
		s.State.mtx.Unlock()

		s.notifyStopped()

		// Will the process be launched again?
		if s.Restart.FromStatus(exit) {
			s.Start(ctx, nil)
			s.State.mtx.Lock()
			s.State.RestartCount++
			s.State.mtx.Unlock()
		}

		break
	}
}

func (s *Service) Stop(ctx context.Context) error {
	// Ensure service is not restarted (also if restart policy mandates it)
	if s.cancelWatch != nil { // Maybe service was not started
		s.cancelWatch()
	}

	if !s.IsProcessAlive() {
		return nil // Cannot stop it bc it was already stopped
	}

	log := s.log()

	killSignal := syscall.SIGTERM
	if s.KillSignal > 0 {
		killSignal = s.KillSignal
		log.Info(ctx, "Killing main PID for service", slog.F("kill-signal", killSignal.String()))
	}

	err := s.Process.Signal(killSignal)
	if err != nil {
		return fmt.Errorf("cannot stop main process for service %s: %w", s.Name, err)
	}

	done := make(chan struct{})
	go s.waitProcess(done)

	select {
	case <-ctx.Done():
		s.Process.Kill()
		err = ctx.Err()
	case <-done:
	}

	s.State.mtx.Lock()
	defer s.State.mtx.Unlock()
	s.State.Started = false
	// We don't mind if process has failed. It was stopped

	// Notify subscribers this process has stopped
	go s.notifyStopped()

	// Consume all remaining messages in deadChildren channel
	for {
		select {
		case <-s.deadChildren:
			continue
		default:
		}
		break
	}

	return err
}

func (s *Service) log() slog.Logger {
	return logger.With(slog.F("service", s.Name))
}

func (s *Service) waitProcess(done chan struct{}) {
	defer close(done)

	if _, err := s.Process.Wait(); err != nil {
		s.log().Error(context.Background(), fmt.Sprintf("Error waiting for service: %s", err))
	}
}

func (s *Service) IsProcessAlive() bool {
	s.serviceMutex.RLock()
	defer s.serviceMutex.RUnlock()
	if s.Process == nil {
		return false
	}

	err := s.Process.Signal(syscall.Signal(0))

	return err == nil // Process ID exists and is not a zombie process
}

// waitStart will wait until ctx is canceled or service changes its state from stopped
func (s *Service) waitStart(ctx context.Context) error {
	if s.IsProcessAlive() {
		return nil
	}

	var timer *time.Timer

	for {
		if timer != nil {
			select {
			case <-timer.C:
			case <-ctx.Done():
				return ctx.Err()
			}
		}
		err := s.State.Err()
		switch {
		case errors.Is(err, ErrNotStarted):
			timer = time.NewTimer(time.Second)
			continue
		case errors.Is(err, ErrFailed) || errors.Is(err, ErrBadConfig):
			return fmt.Errorf("%s: %w", s.Name, err)
		default:
			return err
		}
	}
}

type ServiceState struct {
	Started      bool
	Failed       bool
	BadConfig    bool
	RestartCount int

	mtx sync.Mutex
}

func (ss *ServiceState) IsRunning() bool {
	ss.mtx.Lock()
	defer ss.mtx.Unlock()

	return ss.Started && !ss.Failed && !ss.BadConfig
}

func (ss *ServiceState) ErrorBadConfig() {
	ss.mtx.Lock()
	defer ss.mtx.Unlock()
	ss.BadConfig = true
}

// Err returns an error depending on the actual state of the service
func (ss *ServiceState) Err() error {
	ss.mtx.Lock()
	defer ss.mtx.Unlock()
	switch {
	case ss.BadConfig:
		return ErrBadConfig
	case ss.Failed:
		return ErrFailed
	case !ss.Started:
		return ErrNotStarted
	}

	return nil
}

func (ss *ServiceState) String() string {
	ss.mtx.Lock()
	defer ss.mtx.Unlock()
	var builder strings.Builder
	var particle string
	if !ss.Started {
		particle = "n't"
	}
	fmt.Fprintf(&builder, "Service was%s started\n", particle)
	if !ss.Failed {
		particle = "n't"
	}
	fmt.Fprintf(&builder, "Process did%s fail\n", particle)
	if ss.BadConfig {
		fmt.Fprintln(&builder, "OMG, service config is utterly borked")
	}
	fmt.Fprintf(&builder, "Restart count is at %d\n", ss.RestartCount)
	return builder.String()
}
