package services

import (
	"context"
	"fmt"
	"io"
	"time"
)

// Handle service commands
func (sm *ServiceManager) HandleServiceCommand(command, args string, result io.WriteCloser) {
	defer result.Close()

	if args == "" {
		fmt.Fprintf(result, "Usage: %s <service name>\n", command)
		return
	}

	// First, identify service
	service, exists := sm.mappedServices[args]
	if !exists {
		fmt.Fprintf(result, "Cannot find service name %s. Use the list command to get a list of names\n", args)
		return
	}

	// What may we allow user to do with service?
	var possibleCommands = []string{"restart", "status", "dump"}
	if service.IsProcessAlive() {
		possibleCommands = append(possibleCommands, "stop")
	} else {
		possibleCommands = append(possibleCommands, "start")
	}

	service.State.mtx.Lock()
	if service.State.BadConfig {
		fmt.Fprintln(result, "Bruh, your service is broken")
		possibleCommands = []string{"status"}
	}
	service.State.mtx.Unlock()

	var commandFound bool
	for _, pc := range possibleCommands {
		if pc == command {
			commandFound = true
			break
		}
	}

	if !commandFound {
		fmt.Fprintf(result, "Cannot run command %s on the current state. Available commands are %v\n", command, possibleCommands)
		return
	}

	// Context for operations supporting it
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	switch command {
	case "status":
		fmt.Fprintf(result, "Current status for %s is %s", service.Name, service.State.String())
	case "dump":
		service.State.mtx.Lock()
		fmt.Fprintf(result, "%+v\n", service)
		service.State.mtx.Unlock()
	case "restart", "stop":
		err := service.Stop(ctx)
		if err != nil {
			fmt.Fprintf(result, "Error stopping process: %s\n", err)
		} else {
			fmt.Fprintf(result, "Main process for service %s stopped OK\n", service.Name)
		}
		if command != "restart" {
			break
		}
		fallthrough
	case "start":
		// Start needs a long-running context (like the one defined on init)
		// Hopefully, we save it on the struct, phew!
		if err := service.Start(sm.ctx, GenerateJournal(service, sm.pluginList)); err != nil {
			fmt.Fprintf(result, "Error starting process for service %s: %s\n", service.Name, err)
		} else {
			fmt.Fprintf(result, "Service %s started OK\n", service.Name)
		}
	}
}

func (sm *ServiceManager) HandleListCommand(_, _ string, out io.WriteCloser) {
	defer out.Close()
	for i, service := range sm.services {
		fmt.Fprintf(out, "%3d\t[Started %t] %20s\t%-50s\n", i, service.IsProcessAlive(), service.Name, service.Description)
	}
}

func (sm *ServiceManager) HandleJournalCommands(command, args string, out io.WriteCloser) {
	defer out.Close()

	if args == "" {
		fmt.Fprintf(out, "Usage: %s <service-name>\n", command)
		return
	}

	service, exists := sm.mappedServices[args]
	if !exists {
		fmt.Fprintf(out, "Service %q does not exist. Use the list command\n", args)
		return
	}

	service.serviceMutex.RLock()
	journal := service.Journal
	service.serviceMutex.RUnlock()

	if journal == nil {
		fmt.Fprintf(out, "Service %q doesn't have attached journal. Maybe didn't start?\n", service.Name)
		return
	}

	journal.WriteTo(out)
	out.Write([]byte{'\n'}) // Just in case

	if command == "journal-f" {
		ids, channel := journal.SubscribeTo(true, true)
		defer journal.DeleteSubscription(ids)

		for line := range channel {
			if _, err := out.Write([]byte(line.Format())); err != nil {
				break
			}
		}
	}
}

func (sm *ServiceManager) HandleReloadCommand(_, _ string, out io.WriteCloser) {
	defer out.Close()

	if err := sm.Reload(sm.ctx); err != nil {
		fmt.Fprintf(out, "Cannot reload services: %s\n", err)
		return
	}

	fmt.Fprintf(out, "Reloaded %d services\n", len(sm.services))
}
